class DecryptController < ApplicationController
  def index
    #params.require(:cipher, :enc_text)
    
    cons = "abcdefghijklmnopqrstuvwxyz"
    cipher = params[:cipher]
    cipher_arr = cipher.blank? ? '' : cipher.split("")
  
    enc_text = params[:enc_text]
    enc_text_arr = enc_text.blank? ? '' : enc_text.split("")

    text = ''
    if enc_text_arr != '' && cipher_arr != ''
      enc_text_arr.each do |str|
        strx = cipher_arr.index(str).blank? ? -1 : cipher_arr.index(str)
        if (strx < 0)
          text = text + str
          next
        end
        text = text + cons[strx];
      end
    end

    flash[:decrypted] = "Original Text: " + text
    flash[:notice] = "Text was successfully decrypted."
  end
end
