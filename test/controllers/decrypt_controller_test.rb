require 'test_helper'

class DecryptControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get decrypt_index_url
    assert_response :success
  end

  test "should return 'car'" do
    get decrypt_index_url, params: {:cipher => 'zodvqukgwefbyitmrsplhacxnj', :enc_text => "dzs"}
    
    assert_select "p", "Original Text: car"
  end

  test "should return 'why so serious?'" do
    get decrypt_index_url, params: {:cipher => 'xipmuzfkbrlwotjancqgveshdy', :enc_text => "skd qj qucbjvq?"}
    
    assert_select "p", "Original Text: why so serious?"
  end

  test "should return 'houston, we have a problem'" do
    get decrypt_index_url, params: {:cipher => 'oephjizkxdawubnytvfglqsrcm', :enc_text => "knlfgnb, sj koqj o yvnewju"}
    
    assert_select "p", "Original Text: houston, we have a problem"
  end
end
